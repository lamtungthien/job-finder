/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

/**
 *
 * @author 
 */
public interface EmployeeManagement extends Remote{
    public Vector getInitialData(String fileName) throws RemoteException;
    public boolean saveList(String fileName, Vector data) throws RemoteException;
}
