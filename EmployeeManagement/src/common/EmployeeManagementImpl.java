/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EmployeeManagementImpl extends UnicastRemoteObject implements EmployeeManagement {

    public EmployeeManagementImpl() throws RemoteException {
    }

    @Override
    public Vector getInitialData(String fileName) throws RemoteException {
        Vector data = new Vector(0);
        try{
            FileReader f = new FileReader(fileName);
            BufferedReader br = new BufferedReader(f);
            String line;
            StringTokenizer stk;
            String code, name;
            int salary;
            while((line = br.readLine())!= null){
                stk = new StringTokenizer(line,",");
                Vector v = new Vector();
                v.add(stk.nextToken().trim()); 
                v.add(stk.nextToken().trim()); 
                v.add(Integer.parseInt(stk.nextToken().trim()));
                data.add(v);
            }
            br.close(); f.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public boolean saveList(String fileName, Vector data) throws RemoteException {
        try{
            FileWriter f = new FileWriter(fileName);
            PrintWriter pw = new PrintWriter(f);
            for (int i = 0; i < data.size(); i++) {
                Vector v = ((Vector)(data.get(i)));
                String S = "";
                S += v.get(0) + "," + v.get(1) + "," + v.get(2);
                pw.println(S);
                
            }pw.close(); f.close();
            return true;
            
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
    
   
    
}
